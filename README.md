# Telenor Test Assignment

## Appointments scheduling system

The basic idea of the system to help Telenor service agent to schedule appointments. The appointemtn time will be used to call or chat with one customer at Telenor.

Your responsibility in the test-assingment is to write a web application based on .Net (Framework or core) with frontend that is built as single page application.

The front end and the back end could be in one project or two separated projects.

## Requirements
- ### Backend
1. As service agent should be able to add, update and delete appointment.
1. When adding a new appointment/s is to check if there is any conflict with an existing appointment and return the proper error message such as SamePeriod, OverlapsWith and IntersectsWith.
2. Find a way to to import a list of appointment from the provided Json file.
3. It would be valuable if you can provide unit/integration tests.

- ### Frontend: You can use any JavaScript framework (Angular, React, Vue, etc)
1. A single web page application (separated/integrated with the backend application).
2. In the web page the user can see all the appointments he/she has.
3. The agent can upload a list of appointments from the provided json file.
4. The agent should be able to add new, update or delete existing appointment.

- - - -

## Evaluation

-	Finishing the assignment is valuable, but the main point for us is to evaluate structure and the quality of the code.
-	You can upload your code to  GitHub/Bitbucket repo or send to us in a zip file.
-	Time consumed to finish the task is highly evaluated as well.


Best of luck!

### Telenor.se Team
